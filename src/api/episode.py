import requests


def api_create_episode(name):
    data = {"name": name}
    res = requests.post("http://localhost:8087/api/episodes", json=data, headers={
        "Authorization": "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaXNBZG1pbiI6dHJ1ZSwibmFtZSI6ImFkbWluIiwiaWF0IjoxNjAzNTUzNjEzLCJleHAiOjE2MDQxNTg0MTN9.dAdOcUwI1EWbq-Veex49mrz_k8tehpLLtmQPqtihLSVyDmdn8TH6WOXdHuL_QsB12g9Q4YZqRLhnu8wTPbN5Yg"
    })
    assert res.status_code == 200
