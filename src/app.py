import allure
from selene import Browser

from src.pages.LoginPage import LoginPage
from src.pages.MainPage import MainPage


class Application(object):

    def __init__(self, browser: Browser):
        self.browser = browser

    def auth(self):
        self.browser.driver. \
            execute_script('window.localStorage.setItem(arguments[0], arguments[1]);', "access_token",
                           "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaXNBZG1pbiI6dHJ1ZSwibmFtZSI6ImFkbWluIiwiaWF0IjoxNjAzNTUzNjEzLCJleHAiOjE2MDQxNTg0MTN9.dAdOcUwI1EWbq-Veex49mrz_k8tehpLLtmQPqtihLSVyDmdn8TH6WOXdHuL_QsB12g9Q4YZqRLhnu8wTPbN5Yg")

        # self.browser.driver.({
        #     "name": " access_token",
        #     "value": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaXNBZG1pbiI6dHJ1ZSwibmFtZSI6ImFkbWluIiwiaWF0IjoxNjAzNTUzNjEzLCJleHAiOjE2MDQxNTg0MTN9.dAdOcUwI1EWbq-Veex49mrz_k8tehpLLtmQPqtihLSVyDmdn8TH6WOXdHuL_QsB12g9Q4YZqRLhnu8wTPbN5Yg"})

    def login_page(self):
        return LoginPage(self.browser)

    def main_page(self):
        return MainPage(self.browser)
